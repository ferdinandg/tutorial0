package id.ac.ui.tutorial0.model;

import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class Student {
    private String name;
    private String npm;
    private String address;

}
